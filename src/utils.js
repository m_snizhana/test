function doesNodeBelongToContainer(node, container) {
    while (node) {
        if (node === container) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

function doesSelectionBelongToContainer(el, selection) {
    if (selection.rangeCount) {
        for (let i = 0; i < selection.rangeCount; i++) {
            if (!doesNodeBelongToContainer(selection.getRangeAt(i).commonAncestorContainer, el)) {
                return false;
            }
        }
        return true;
    }
    return false;
}

export { doesSelectionBelongToContainer };
