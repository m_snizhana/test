import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types'
import './FileZone.css';

class FileZone extends Component {
    static propTypes = {
        nodes: PropTypes.array.isRequired
    };


    componentDidUpdate() {
        const { nodes } = this.props;
        const text = document.getElementById('file').innerText;
        
        for (let { start, end, classes } of nodes) {
            const subst = text.substring(start, end);
            const chars = text.split('');
            const el = `<span class="${classes.join(' ')}">${subst}</span>`;
            
            chars.splice(start, end-start, el);
            document.getElementById('file').innerHTML = chars.join('');
        }

    }

    render() {
        return (
            <div id="file-zone">
                <div id="file" contentEditable ref={this.file} />
            </div>
        );
    }
}

export default FileZone;
