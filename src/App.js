import React, {Component} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from './text.service';
import { doesSelectionBelongToContainer } from './utils';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nodes: []
        };
    }

    handleClick = prop => {
        const selection = window.getSelection();
        const text = selection.toString();
        const el = document.getElementById("file");
        const isvalid = doesSelectionBelongToContainer(el, selection);

        if (text && isvalid) {
            const [ start, end ] = selection.anchorOffset < selection.extentOffset 
                ? [ selection.anchorOffset,  selection.extentOffset ] : [ selection.extentOffset, selection.anchorOffset ]
            const selectionRange = { start, end };

            this.setState(prevState => {
                const targedNode = this.findNode(prevState, selectionRange) || { classes: [] };

                return {
                    nodes: [
                        ...prevState.nodes.filter(node => node !== targedNode),
                        {
                            ...selectionRange,
                            classes: this.getClassNames(prop, targedNode.classes)
                        }
                    ],
                    selection: text
                };
            });
        }
    }

    getText() {
        getMockText().then(function (result) {
            console.log(result);
        });
    }

    findNode(state, { start, end }) {
        return state.nodes.find(node => node.start === start && node.end === end);
    }

    getClassNames(prop, classes) {
        if (classes.includes(prop)) {
            return classes.filter(c => c !== prop);
        }

        return [ ...classes, prop ];
    }

    render() {
        const { nodes } = this.state;
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel handleClick={this.handleClick} />
                    <FileZone nodes={nodes} />
                </main>
            </div>
        );
    }
}

export default App;
