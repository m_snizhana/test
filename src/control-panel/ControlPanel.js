import React, { Component } from 'react';
import './ControlPanel.css';

class ControlPanel extends Component {
    render() {
        const { handleClick } = this.props;

        return (
            <div id="control-panel">
                <div id="format-actions">
                    <button onClick={() => handleClick('bold')} className="format-action" type="button"><b>B</b></button>
                    <button onClick={() => handleClick('italic')} className="format-action" type="button"><i>I</i></button>
                    <button onClick={() => handleClick('underline')} className="format-action" type="button" name="underline"><u>U</u></button>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
